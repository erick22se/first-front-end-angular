export { ICategory } from './category';
export { IProduct } from './product';
export { ISaleItem } from './saleItem';