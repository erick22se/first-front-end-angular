import { IProduct } from '.';

export interface ISaleItem {
    id?: number;
    quantity: number;
    product: IProduct;
}