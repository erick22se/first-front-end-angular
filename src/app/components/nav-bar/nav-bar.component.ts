import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  filter = '';

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onHomeSelect(): void {
    this.router.navigate(['']);
  }

  setFilter(e): void {
    if (e.keyCode == 13) {
      this.router.navigate(['/buscar-producto'], { queryParams: { filter: this.filter } });
    }
  }

  goToShoppingCart(): void {
    this.router.navigate(['/mi-carrito-de-compras']);
  }

  onClickBars(): void {
    const elements = document.getElementsByClassName('bar');
    const mButton = document.getElementById('menuButton-main');
    for (let i = 0; i < elements.length; i++) {
      if (elements[i].className.includes('change')) {
        elements[i].className = elements[i].className.replace(' change', '');
        mButton.className = mButton.className.replace(' change', '');
      } else {
        elements[i].className += ' change';
        mButton.className += ' change';
      }
    }

  }
}
