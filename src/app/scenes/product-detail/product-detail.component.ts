import { Component, OnInit } from '@angular/core';
import { IProduct, ISaleItem } from 'src/common/entities';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  public product: IProduct;
  public quantity = '1';

  constructor(private route: ActivatedRoute, private productService: ProductService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        const id: number = parseInt(params['id']);
        this.product = this.productService.findById(id);
      });
  }

  addToShoppingCar(): void {
    if (/^[1-9][0-9]*$/.test(this.quantity)) {
      const saleItem: ISaleItem = {
        quantity: Number(this.quantity),
        product: this.product
      };
      this.productService.pushToBuy(saleItem);
      this.messageService.add({ severity: 'success', summary: 'Añadido', detail: 'El producto se añadió a tu carrito' });
    } else {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Ingrese una cantidad correcta' });
    }
  }

}
