import { Component, OnInit } from '@angular/core';
import { ISaleItem } from 'src/common/entities';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  public saleItems: ISaleItem[] = []

  constructor(private productService: ProductService) {
  }

  ngOnInit(): void {
    this.saleItems = this.productService.productsToBuy;
    console.log(this.saleItems);
  }

  onDeleteItem(id: number): void {
    this.saleItems = this.productService.removeItemToBuy(id);
  }

}
