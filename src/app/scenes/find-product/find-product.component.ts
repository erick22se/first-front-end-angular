import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IProduct } from 'src/common/entities';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-find-product',
  templateUrl: './find-product.component.html',
  styleUrls: ['./find-product.component.css']
})
export class FindProductComponent implements OnInit {

  public products: IProduct[] = [];
  public responsiveOptions: any[];
  public filter = '';

  constructor(private route: ActivatedRoute, private productService: ProductService, private router: Router) {
    this.responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 3
      },
      {
        breakpoint: '768px',
        numVisible: 2,
        numScroll: 2
      },
      {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1
      }
    ];
  }

  ngOnInit(): void {
    this.route.queryParamMap
      .subscribe((params) => {
        this.filter = params.get('filter');
        this.products = this.productService.filterProducts(this.filter);
      }
      );
  }

  onProductSelect(id: number): void {
    this.router.navigate([`productos/${id}/detalle`]);
  }
}
