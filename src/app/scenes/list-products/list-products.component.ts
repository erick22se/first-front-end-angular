import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Categories } from '../../../common/constants';
import { IProduct } from '../../../common/entities';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit {

  public categoryName: string;
  public products: IProduct[] = [];
  public responsiveOptions: any[];

  constructor(private route: ActivatedRoute, private router: Router, private productService: ProductService) {
    this.responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 3
      },
      {
        breakpoint: '768px',
        numVisible: 2,
        numScroll: 2
      },
      {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1
      }
    ];
  }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        const id: number = parseInt(params['categoryId']);
        switch (id) {
          case Categories.WHITE_LINE:
            this.categoryName = 'Linea Blanca';
            this.products = this.productService.filterProducts('', id);
            break;
          case Categories.CLOTHES:
            this.categoryName = 'Ropa';
            this.products = this.productService.filterProducts('', id);
            break;
          case Categories.TOYS:
            this.categoryName = 'Juguetes';
            this.products = this.productService.filterProducts('', id);
            break;
          case Categories.COMPUTERS:
            this.categoryName = 'Computadoras';
            this.products = this.productService.filterProducts('', id);
            break;
        }
      });
  }

  onProductSelect(id: number): void {
    this.router.navigate([`productos/${id}/detalle`]);
  }

}
