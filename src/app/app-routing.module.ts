import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListProductsComponent } from './scenes/list-products/list-products.component';
import { ProductDetailComponent } from './scenes/product-detail/product-detail.component';
import { WelcomeComponent } from './scenes/welcome/welcome.component';
import { TermAndCondComponent } from './scenes/term-and-cond/term-and-cond.component';
import { ContactComponent } from './scenes/contact/contact.component';
import { QuestionsComponent } from './scenes/questions/questions.component';
import { FindProductComponent } from './scenes/find-product/find-product.component';
import { ShoppingCartComponent } from './scenes/shopping-cart/shopping-cart.component';

const routes: Routes = [
  {
    path: '',
    component: WelcomeComponent
  },
  {
    path: 'productos-por-categoria/:categoryId',
    component: ListProductsComponent
  },
  {
    path: 'productos/:id/detalle',
    component: ProductDetailComponent
  },
  {
    path: 'terminos-y-condiciones',
    component: TermAndCondComponent,
  },
  {
    path: 'contactenos',
    component: ContactComponent,
  },
  {
    path: 'preguntas-y-respuestas',
    component: QuestionsComponent,
  },
  {
    path: 'buscar-producto',
    component: FindProductComponent,
  },
  {
    path: 'mi-carrito-de-compras',
    component: ShoppingCartComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
