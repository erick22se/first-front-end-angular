import { Injectable } from '@angular/core';
import { IProduct, ISaleItem } from '../../common/entities';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  public products: IProduct[] = [
    {
      id: 1,
      name: 'Product 1',
      price: 16.5,
      category: {
        id: 1,
        name: 'Linea Blanca'
      }
    },
    {
      id: 2,
      name: 'Product 2',
      price: 16.5,
      category: {
        id: 1,
        name: 'Linea Blanca'
      }
    },
    {
      id: 3,
      name: 'Product 3',
      price: 16.5,
      category: {
        id: 1,
        name: 'Linea Blanca'
      }
    },
    {
      id: 4,
      name: 'Product 4',
      price: 16.5,
      category: {
        id: 1,
        name: 'Linea Blanca'
      }
    },
    {
      id: 5,
      name: 'Product 5',
      price: 16.5,
      category: {
        id: 1,
        name: 'Linea Blanca'
      }
    },
    {
      id: 6,
      name: 'Product 6',
      price: 16.5,
      category: {
        id: 2,
        name: 'Ropa'
      }
    },
    {
      id: 7,
      name: 'Product 7',
      price: 16.5,
      category: {
        id: 2,
        name: 'Ropa'
      }
    },
    {
      id: 8,
      name: 'Product 8',
      price: 16.5,
      category: {
        id: 3,
        name: 'Juguetes'
      }
    },
    {
      id: 9,
      name: 'Product 9',
      price: 16.5,
      category: {
        id: 4,
        name: 'Computadora'
      }
    },
    {
      id: 10,
      name: 'Product 10',
      price: 16.5,
      category: {
        id: 4,
        name: 'Computadora'
      }
    },
    {
      id: 11,
      name: 'Product 11',
      price: 16.5,
      category: {
        id: 4,
        name: 'Computadora'
      }
    },
    {
      id: 12,
      name: 'Product 12',
      price: 16.5,
      category: {
        id: 4,
        name: 'Computadora'
      }
    }
  ];

  public productsToBuy: ISaleItem[] = [];

  constructor() { }

  filterProducts(filter = '', categoryId = 0): IProduct[] {
    let data: IProduct[] = [];

    if (filter.trim().length > 0) {
      data = this.products.filter(x => x.name.includes(filter));
    }

    if (categoryId != 0) {
      data = this.products.filter(x => x.category.id == categoryId);
    }

    return data;
  }

  findById(productId): IProduct {
    return this.products.find(x => x.id == productId);
  }

  pushToBuy(item: ISaleItem): void {
    item.id = this.productsToBuy.length + 1;
    this.productsToBuy.push(item);
  }

  removeItemToBuy(itemId: number): ISaleItem[] {
    this.productsToBuy = this.productsToBuy.filter(x => x.id != itemId);
    return this.productsToBuy;
  }
}
