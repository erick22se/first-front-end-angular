import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { FooterComponent } from './components/footer/footer.component';
import { ListProductsComponent } from './scenes/list-products/list-products.component';
import { ProductDetailComponent } from './scenes/product-detail/product-detail.component';
import { WelcomeComponent } from './scenes/welcome/welcome.component';
import { CarouselModule } from 'primeng/carousel';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { TermAndCondComponent } from './scenes/term-and-cond/term-and-cond.component';
import { ContactComponent } from './scenes/contact/contact.component';
import { QuestionsComponent } from './scenes/questions/questions.component';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { AccordionModule } from 'primeng/accordion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FindProductComponent } from './scenes/find-product/find-product.component';
import { FormsModule } from '@angular/forms';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { ShoppingCartComponent } from './scenes/shopping-cart/shopping-cart.component';
import { TableModule } from 'primeng/table';
import { CdkScrollableModule } from '@angular/cdk/scrolling';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    FooterComponent,
    ListProductsComponent,
    ProductDetailComponent,
    WelcomeComponent,
    TermAndCondComponent,
    ContactComponent,
    QuestionsComponent,
    FindProductComponent,
    ShoppingCartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CarouselModule,
    InputTextModule,
    InputTextareaModule,
    AccordionModule,
    ButtonModule,
    BrowserAnimationsModule,
    FormsModule,
    ToastModule,
    TableModule,
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
